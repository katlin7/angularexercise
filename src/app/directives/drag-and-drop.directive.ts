import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[appDragAndDrop]'
})
export class DragAndDropDirective {

  error: string = '';
  second_file = null;
  @Output() fileDropped = new EventEmitter<any>();
  @HostBinding('class.fileover') fileOver: boolean | undefined;

  @HostListener('dragover', ['$event']) public onDragOver(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.fileOver = true;
    console.log("Drag over");

  }

  @HostListener('dragover', ['$event']) public onDragLeave(evt: any) {
    evt.preventDefault();
    evt.stopPropagation();
    this.fileOver = false;
    console.log("Drag leave");
  }

  @HostListener('drop', ['$event']) public onDrop(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();

    const files: FileList = evt.dataTransfer!.files;
    if (files.length > 0) {
      this.fileOver = false;
      let files = evt.dataTransfer!.files;
      if (files.length === 1) {
        this.fileDropped.emit(files);
        console.log(evt);
      } else {
        this.fileDropped.emit(null);
      }
    }
  }
}
