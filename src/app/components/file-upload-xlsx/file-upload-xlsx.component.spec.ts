import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadXlsxComponent } from './file-upload-xlsx.component';

describe('FileUploadXlsxComponent', () => {
  let component: FileUploadXlsxComponent;
  let fixture: ComponentFixture<FileUploadXlsxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadXlsxComponent]
    });
    fixture = TestBed.createComponent(FileUploadXlsxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
