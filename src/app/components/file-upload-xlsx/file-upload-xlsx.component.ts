
import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-file-upload-xlsx',
  templateUrl: './file-upload-xlsx.component.html',
  styleUrls: ['./file-upload-xlsx.component.css']
})
export class FileUploadXlsxComponent {

  data: any;
  storedData: any[] = [];
  errorMsg: string = '';
  successMsg: string = '';


  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getDataFromLocalStorage();
  }

  getDataFromLocalStorage() {
    if (localStorage.getItem('arrayData') !== null) {
      this.storedData = JSON.parse(localStorage['arrayData']);
    }
    if (localStorage.getItem('dataRow') !== null) {
      localStorage.removeItem('dataRow');
    }
  }

  //drag-and-drop, from directives
  onFileDropped($event: any) {
    this.errorMsg = '';
    this.successMsg ='';
    if ($event === null) {
      this.errorMsg = "Korraga saab üles laadida vaid ühe faili!";
      return;
    }
    this.getFileContent($event);
  }

  //upload from filebrowser
  getFilesToUpload(target: any) {
    this.getFileContent(target.files);
    target.value = null;
  }

  //get content of file
  getFileContent(files: Array<any>) {
    this.readFileContent(files[0]);
  }

  //process file data
  readFileContent(file: any): any {
    this.errorMsg = '';
    this.successMsg ='';
    const fileReader = new FileReader();

    fileReader.onload = (e: any) => {
      const result: string = e.target.result;
      const workbook: XLSX.WorkBook = XLSX.read(result, { type: 'binary' });
      const worksheetName: string = workbook.SheetNames[0];
      const worksheet: XLSX.WorkSheet = workbook.Sheets[worksheetName];

      //map fields names from Excel for the app:
      worksheet['A1'].w = "Andmeelement";
      worksheet['B1'].w = "Kirjeldus";
      worksheet['C1'].w = "Andmesonastiku";
      worksheet['D1'].w = "Arisonastiku1";
      worksheet['E1'].w = "Arisonastiku2";

      this.data = XLSX.utils.sheet_to_json(worksheet);

      //check if strValue is empty string, null, undefined 
      for (let i = 0; i < this.data.length; i++) {
        if (this.data[i].Andmeelement === undefined ||
          this.data[i].Andmeelement === null ||
          String(this.data[i].Andmeelement).trim() === '') {
          this.errorMsg = `Viga andmetes! \'Andmeelement\' reas ${i} puudub, andmeid ei saa lisada!`;
          return;
        }
        if (this.data[i].Kirjeldus === undefined ||
          this.data[i].Andmeelement === null ||
          String(this.data[i].Kirjeldus).trim() === '') {
          this.errorMsg = `Viga andmetes! \'Kirjeldus\' reas ${i + 1} puudub, andmeid ei saa lisada!`;
          return;
        }
        if (this.data[i].Kirjeldus === undefined ||
          this.data[i].Andmeelement === null ||
          String(this.data[i].Andmesonastiku).trim() === '') {
          this.errorMsg = `Viga andmetes! \'Andmesõnastiku termin\' reas ${i + 2} puudub, andmeid ei saa lisada!`;
          return;
        }
      }

      this.errorMsg = '';

      //push data to the array
      for (let i = 0; i < this.data.length; i++) {
        this.storedData.push(this.data[i]);
      }

      this.successMsg = 'Faili üleslaadimine õnnestus!'

      // As there is no backend, I save the data to the local storage 
      localStorage['arrayData'] = JSON.stringify(this.storedData);


    };

    fileReader.readAsBinaryString(file);
    this.successMsg = '';
  }

  //send data to the chart:
  sendRowData(value: any): void {
    const array: string[] = new Array;
    array.push(value.Andmeelement);
    array.push(value.Kirjeldus);
    array.push(value.Andmesonastiku);
    array.push(value.Arisonastiku1);
    array.push(value.Arisonastiku2);
    this.dataService.setDataArray(array);
  }
}

