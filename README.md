# AngularOnDocker

Exceli andmete ülesalaadimise leht ja diagrammid.

- Rakendus on täielikult front-end lahendus, kasutab andmete salvestamiseks veebilehitseja LocalStorage'it 
- VIDEO rakendusest (31.03.24): https://youtu.be/Mm7-UVNrHRc 

Rakendus on loodud Angulari versioonil 16.2.0, mis jookseb Node.js versioonil 17.9.1. Diagrammide loomiseks on kasutatud Chart.js v 4.4.2 versiooni https://www.chartjs.org/docs/4.4.2/ ja Chart.js Annotations pluginat (3.0.1) https://www.chartjs.org/chartjs-plugin-annotation/latest/



## Funktsionaalsused

- Exceli faile saab üles laadida nupu abil või lohistada drop-down alale 
- Failide üleslaadimisel lisatakse juba olemasolevatele andmetele andmed uuest failist
- Üleslaadimise õnnestumisel kuvatakse õnnestumise teade
- Vigaste andmete üleslaadimisel (kui Exceli tabeli esimeses kolmes tulbas on mõne välja sisu tühi string, undefined, null), näidatakse veateadet ja andmeid tabelisse ei lisata
- Hiirega Andmeelemendi tulbale liikudes näidatakse andmeelemndi kirjeldust Exceli tabeli teisest tulbast
- Igas tabeli reas on nupp, millele vajutades saab vaadata selle rea andmete seoste diagrammi
- Seosed on omavahel ühendatud joontega
- Seoste diagrammis on iga rea puhul eraldi näha vaid tabeli need väljad, mis tabelis  ontäidetud. Tühje väljasid (ja jooni nendeni) ei kuvata.
- Lehtede vahel saab liikuda nuppude abil (jdiagrammi lehelt ka menüüs 'Esileht' peale vajutades)
- Diagrammi kujundeid saab lohistada ja nendega koos liiguvad ka diagramme ühendavad jooned
- PARANDATUD 30.03: Diagrammi kujundite lohistamisel liiguvad kujunditega kaasa ka neid ühendavad jooned
- PARANDATUD 30.03: Diagrammil näidatakse tooltippi kui hiirega andmeelemendil olevale küsimärgile klõpsata
- PARANDATUD 30.03: Kui drop-down alale lohistada rohkem kui 1 fail korraga, kuvatakse kasutajale teade, et üles saab laadida vaid 1 faili korraga
- PARANDATUD 30.03: Enne tabeli üleslaadimist (kui eelnevaid andmeid veel veebilehe Local Storage'sse salvestatud ei ole) tulpade pealklkirju veebilehel ei näidata
- PARANDATUD 30.03: Mobiilivaate jaoks on lisatud tabeli wrap ja tekst muudetakse väiksemaks

**NB! Ubuntu 22.04 kasutajatele - käivitatud veebilehel drag-and-drop üleslaadimise funktsionaalsuse kasutamiseks:** 

- **Ubuntu poolne viga:** (viga on Ubuntu poolne ega ole tingitud käesoleva programmi koodist) kui veebilehele (localhost:4200) ei õnnestu .XLSX faile üles laadida lohistades, tuleb failide lohistamiseks eelnevalt avada Nautiluse brauser sudo õigustes: 

`sudo nautilus /home/user/` 

või

`sudo xdg-open /home/user/` 

- ja lohistada failid selle abil avanenud failibrauseri aknast veebilehele. 
- Asenda `user`oma arvuti kasutajanimega, näiteks `sudo nautilus /home/kaspar/` või `sudo nautilus /home/kaur/` . 

( Ubuntus veebilehele failide lohistamise probleem on dokumenteeritud: https://askubuntu.com/questions/1421539/drag-and-drop-of-files-not-working-on-version-22-04 ). See drag-and-drop variant toimib Ubuntul nii Chrome kui ka Firefox brauserites (teistes ei ole katsetanud).  

## Hetkel arenduses
- **Hetkel täiendamisel**: koodi ülevaatamine, testimine 


## Käivitusjuhend dockeril (käsurealt)

Käivitamist olen hetkeseisuga katsetanud ja toimib vähemalt Ubuntu 22.04 masinas ja Windows 11 peal.

- Eelduseks Dockeri olemasolu arvutis ( https://www.docker.com/ )
- Laadi rakenduse kood alla: `git clone https://gitlab.com/katlin7/angularexercise.git` 
- Mine allalaaditud rakenduse juurkausta, kus asuvad Dockerfile ja docker-compose.yml
- Käivita käsurealt: 

`docker compose up -d` 

või 

`docker compose up` 

(Mõnes arvutis võib piirangute tõttu olla vajalik käivitada docker compose up sudo õigustes: `sudo docker compose up -d`)

- Docker compose abil ehitatakse rakendus automaatselt üles (kasutades Dockerfile'i ja nginx.conf faili) ja käivitatakse.
- Veebilehe nägemiseks tuleb brauseris navigeerida aadressile: http://localhost:4200



## Käivitusjuhend ilma dockerita:

- Käivitamist olen katsetanud ja toimib Windows 11 ja Ubuntu 22.04 masinas
- Eeldab Node.js ja Angulari olemasolu arvutis. Node.js versioon, mida kasutasin, on 17.9.1 ja Angularil 16.2.0 (teiste versioonidega töötamist ei garanteeri)
- Node ja Angulari sobivate versioonide installijuhend on pealkirja "Node v17.9.1 ja Angular 16.2.0 install" all <a href="#node-v1791-ja-angular-1620-install">Node v17.9.1 ja Angular 16.2.0 install</a>
    
- **Kui arvutisse on juba installitud Node 17.9.1 ja Angular 16.2.0** laadi rakenduse kood alla: `git clone https://gitlab.com/katlin7/angularexercise.git`
- Ava arvutis (näiteks terminalis või VSCode programmiga) alla laaditud rakenduse juurkaust "angularexercise" (sama kaust, kus asub Dockerfile)
- Ava VSCode programmis terminali aken (või ava juurkaustas Ubuntu terminal)
- Kirjuta terminali aknasse `npm install` (see installib rakenduse käivitamiseks vajalikud paketid) 
- Kui install on lõppenud, kirjuta terminali aknasse `ng serve -o` - see käivitab serveri ja avab angulari lehe aadressil http://localhost:4200


## Node v17.9.1 ja Angular 16.2.0 install
- **Node.js v17.9.1 install Ubuntu versioonile 22.04** (https://tecadmin.net/how-to-install-nvm-on-ubuntu-22-04/): 
        - `sudo apt update`
        - kui arvutis ei ole curl, installi curl: `sudo apt install curl`
        - kui arvutis ei ole nvm-i, installi nvm (lubab kasutada samas arvutis erinevaid Node versioone): `curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash`
        - installitud keskkonna laadimiseks logi arvutist oma kasutaja alt välja + logi sisse (või sisesta käsurealt: `source ~/.profile` , mis teeb sama töö ära)
        - kui arvutis on nvm (varasemalt või äsja installitud), installi node versioon 17.9.1: `nvm install 17.9.1` (või kui eelmine ei toimi, siis `nvm install 17.9.1`) 
        - vali versioon: `nvm use 17.9.1` (või kui eelmine ei toimi, siis `nvm use v17.9.1` ) 
    - **Angular 16 install** lokaalselt (hea kasutada, kui arvutis on eelnevalt juba mõni Angulari versioon): 
        - Mine kausta, kuhu soovid installida Angulari vastava versiooni, nt /Home/<USERNAME>/Documents/Angular16 
        - Ava selles kaustas terminal ja sisesta: `npm i @angular/cli@16.2.0`
        - NB! Laadi rakenduse https://gitlab.com/katlin7/angularexercise.git kood eelmises punktis mainitud kausta nt `git clone https://gitlab.com/katlin7/angularexercise.git`
    - **NVM abil Node.js 17.9.1 install Windows'ile** (valikuline):
        - Installi NVM erinevate Node versioonide kasutamiseks: https://github.com/coreybutler/nvm-windows?tab=readme-ov-file#overview
        - Ava Windows cmd
        - Node 17.9.1 kasutamiseks:
        - `nvm install 17.9.1` (või kui see ei tööta, siis `nvm install v17.9.1`)
        - `nvm use 17.9.1`  (või kui see ei tööta, siis `nvm use v17.9.1`)
    - **Angular 16 install Windowsile lokaalselt (vt eelmisi punkte)**

## Ekraanipildid

![nodata.png](src/assets/nodata.png)
![tabel.png](src/assets/tabel.png)
![tooltips.png](src/assets/tooltips.png)
![tooltips2.png](src/assets/tooltips2.png)
![success.png](src/assets/success.png)
![viga1.png](src/assets/viga1.png)
![viga2.png](src/assets/viga2.png)
![diagramm.png](src/assets/diagramm.png)
![diagramm3.png](src/assets/diagramm3.png)
![diagramm4.png](src/assets/diagramm4.png)
![diagramm5.png](src/assets/diagramm5.png)